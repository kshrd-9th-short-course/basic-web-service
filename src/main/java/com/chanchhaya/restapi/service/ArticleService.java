package com.chanchhaya.restapi.service;

import java.util.List;

import com.chanchhaya.restapi.data.dto.ArticleDto;

public interface ArticleService {
    
    List<ArticleDto> findAll();
    ArticleDto findByUUID(String uuid);
    ArticleDto save(ArticleDto article);
    ArticleDto updateByUUID(ArticleDto article);
    ArticleDto updateStatusToFalse(String uuid);
    ArticleDto deleteByUUID(String uuid);

}
