package com.chanchhaya.restapi.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import com.chanchhaya.restapi.service.ImageUploadService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageUploadServiceImpl implements ImageUploadService {

    @Value("${image.server-path}")
    private String serverPath;

    @Override
    public String upload(MultipartFile file) {

        String extension = "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);

        String name = UUID.randomUUID().toString() + extension;
        
        try {
            Path path = Paths.get(serverPath + name);
            Files.copy(file.getInputStream(), path);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not upload file " + file.getOriginalFilename());
        }

        return name;

    }
    
}
