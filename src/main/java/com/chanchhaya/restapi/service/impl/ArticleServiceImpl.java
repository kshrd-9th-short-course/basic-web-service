package com.chanchhaya.restapi.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.chanchhaya.restapi.data.domain.Article;
import com.chanchhaya.restapi.data.dto.ArticleDto;
import com.chanchhaya.restapi.data.repository.ArticleRepository;
import com.chanchhaya.restapi.service.ArticleService;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ArticleServiceImpl implements ArticleService {
    
    Logger logger = LoggerFactory.getLogger(ArticleServiceImpl.class);
    private ArticleRepository articleRepository;
    private ModelMapper mapper;

    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository, ModelMapper mapper) {
        this.articleRepository = articleRepository;
        this.mapper = mapper;
    }

    @Override
    public ArticleDto save(ArticleDto article) {

        Article articleDomain = mapper.map(article, Article.class);
        logger.info("articleDomain = " + articleDomain);

        try {

            String uuid = UUID.randomUUID().toString();
            articleDomain.setUuid(uuid);

            articleRepository.save(articleDomain);

            return mapper.map(articleRepository.findByUUID(uuid), ArticleDto.class);

        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }

    }

    @Override
    public List<ArticleDto> findAll() {

        List<ArticleDto> result = new ArrayList<>();

        List<Article> articles = articleRepository.findAll();

        for (Article article : articles) {
            result.add(mapper.map(article, ArticleDto.class));
        }

        return result;

    }

    @Override
    public ArticleDto updateByUUID(ArticleDto article) {
        
        Article articleDomain = mapper.map(article, Article.class);

        logger.info("articleDomain = " + articleDomain);

        try {

            articleRepository.update(articleDomain);

            return mapper.map(articleRepository.findByUUID(article.getUuid()), ArticleDto.class);
            
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
            ex.getMessage());
        }
    }

    @Override
    public ArticleDto updateStatusToFalse(String uuid) {
        try {
            ArticleDto articleDto = mapper.map(articleRepository.findByUUID(uuid), ArticleDto.class);

            articleRepository.updateStatusToFalse(uuid);

            return articleDto;
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @Override
    public ArticleDto deleteByUUID(String uuid) {
        try {

            Article articleDomain = articleRepository.findByUUID(uuid);

            ArticleDto articleDto;

            if (articleDomain == null) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Article not found");
            } else {
                articleDto = mapper.map(articleDomain, ArticleDto.class);
            }

            articleRepository.deleteByUUID(uuid);

            return articleDto;
            
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    @Override
    public ArticleDto findByUUID(String uuid) {
        
        try {

            Article articleDomain = articleRepository.findByUUID(uuid);

            if (articleDomain == null) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Article not found");
            }

            ArticleDto article = mapper.map(articleDomain, ArticleDto.class);

            return article;

        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }

    }

}
