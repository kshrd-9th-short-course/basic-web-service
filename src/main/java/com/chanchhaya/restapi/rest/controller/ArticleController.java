package com.chanchhaya.restapi.rest.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.chanchhaya.restapi.data.dto.ArticleDto;
import com.chanchhaya.restapi.exception.AppException;
import com.chanchhaya.restapi.rest.request.ArticleRequest;
import com.chanchhaya.restapi.rest.response.ArticleResponse;
import com.chanchhaya.restapi.rest.response.BaseResponse;
import com.chanchhaya.restapi.rest.response.ErrorResponse;
import com.chanchhaya.restapi.service.impl.ArticleServiceImpl;
import com.chanchhaya.restapi.shared.components.ApiConstants;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(ApiConstants.API_VERSION + ApiConstants.API_END_POINT_ARTICLES)
public class ArticleController {

    private ArticleServiceImpl articleService;
    private ModelMapper mapper;

    @Autowired
    public ArticleController(ArticleServiceImpl articleService, ModelMapper mapper) {
        this.articleService = articleService;
        this.mapper = mapper;
    }

    Logger logger = LoggerFactory.getLogger(ArticleController.class);

    @PutMapping("/{uuid}")
    public ResponseEntity<BaseResponse<ArticleResponse>> updateArticleByUUID(
        @PathVariable("uuid") String uuid,
        @Valid @RequestBody ArticleRequest request
    ) {

        logger.info("Request = " + request);

        BaseResponse<ArticleResponse> response = new BaseResponse<>();

        ArticleDto articleDto = mapper.map(request, ArticleDto.class);
        articleDto.setUuid(uuid);

        logger.info("articleDto = " + articleDto);

        articleDto = articleService.updateByUUID(articleDto);

        response.setCode(9999);
        response.setMessage("Article is updated successfully");
        response.setData(mapper.map(articleDto, ArticleResponse.class));
        response.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(response);

    }

    @PostMapping
    public ResponseEntity<BaseResponse<ArticleResponse>> addNewArticle(
        @Valid @RequestBody ArticleRequest request) {

        BaseResponse<ArticleResponse> response = new BaseResponse<>();

        ArticleDto articleDto = mapper.map(request, ArticleDto.class);
        
        articleDto = articleService.save(articleDto);

        response.setCode(9999);
        response.setMessage("Article is saved successfully");
        response.setData(mapper.map(articleDto, ArticleResponse.class));
        response.setStatus(HttpStatus.OK);
        
        return ResponseEntity.ok(response);

    }

    @GetMapping
    public ResponseEntity<BaseResponse<List<ArticleResponse>>> findAllArticles() {

        BaseResponse<List<ArticleResponse>> response = new BaseResponse<>();

        List<ArticleResponse> articleResponse = new ArrayList<>();

        List<ArticleDto> articleList = articleService.findAll();
        for (ArticleDto articleDto : articleList) {
            articleResponse.add(mapper.map(articleDto, ArticleResponse.class));
        }

        response.setCode(200);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Articles have been found successfully");
        response.setData(articleResponse);


        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{uuid}/disable")
    public ResponseEntity<BaseResponse<ArticleResponse>> updateStatusToFalse(@PathVariable("uuid") String uuid) {

        BaseResponse<ArticleResponse> response = new BaseResponse<>();

        ArticleResponse article = mapper.map(articleService.updateStatusToFalse(uuid), ArticleResponse.class);

        response.setCode(9999);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Articles have been disabled successfully");
        response.setData(article);

        return ResponseEntity.ok(response);

    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<BaseResponse<ArticleResponse>> deleteByUUID(@PathVariable("uuid") String uuid) {
        
        BaseResponse<ArticleResponse> response = new BaseResponse<>();

        ArticleResponse article = mapper.map(articleService.deleteByUUID(uuid), ArticleResponse.class);

        response.setCode(9999);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Articles have been deleted successfully");
        response.setData(article);

        return ResponseEntity.ok(response);

    }

    @GetMapping("/{uuid}")
    ResponseEntity<BaseResponse<ArticleResponse>> findByUUID(@PathVariable("uuid") String uuid) {

        BaseResponse<ArticleResponse> response = new BaseResponse<>();

        ArticleResponse article = mapper.map(articleService.findByUUID(uuid), ArticleResponse.class);

        response.setCode(9999);
        response.setStatus(HttpStatus.OK);
        response.setMessage("Article has been found successfully");
        response.setData(article);

        return ResponseEntity.ok(response);

    }

}
