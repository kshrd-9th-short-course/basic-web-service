package com.chanchhaya.restapi.rest.controller;

import java.util.ArrayList;
import java.util.List;

import com.chanchhaya.restapi.service.impl.ImageUploadServiceImpl;
import com.chanchhaya.restapi.shared.components.ApiConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping(ApiConstants.API_VERSION + ApiConstants.API_END_POINT_IMAGES)
public class ImageUploadController {

    private final ImageUploadServiceImpl imageUploadServiceImpl;

    @Autowired
    public ImageUploadController(ImageUploadServiceImpl imageUploadServiceImpl) {
        this.imageUploadServiceImpl = imageUploadServiceImpl;
    }

    @Operation(summary = "This end-point is used to upload a single image")
    @PostMapping(value = "/upload-single",
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> uploadSingle(@RequestParam("image") MultipartFile image) {

        String imageName = imageUploadServiceImpl.upload(image);

        return ResponseEntity.ok(imageName);

    }

    @PostMapping(value = "/upload-multi",
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<List<String>> uploadMulti(@RequestPart MultipartFile[] images) {

        List<String> imageNameList = new ArrayList<>();

        for (MultipartFile image : images) {
            String imageName = imageUploadServiceImpl.upload(image);
            imageNameList.add(imageName);
        }

        return ResponseEntity.ok(imageNameList);

    }

}
