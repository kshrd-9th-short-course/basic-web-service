package com.chanchhaya.restapi.rest.controller;

import com.chanchhaya.restapi.shared.components.ApiConstants;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiConstants.API_VERSION + ApiConstants.API_END_POINT_CATEGORIES)
public class CategoryController {
    
    @GetMapping
    public String getValue() {
        return "Hello Value";
    }

}
