package com.chanchhaya.restapi.rest.response;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;

public class ErrorResponse<E> {
    
    private int code;
    private HttpStatus status;
    private String message;
    private E errors;
    private Timestamp timestamp;


    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setErrors(E errors) {
        this.errors = errors;
    }

    public E getErrors() {
        return errors;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "{" +
            " code='" + getCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", message='" + getMessage() + "'" +
            ", errors='" + getErrors() + "'" +
            ", timestamp='" + getTimestamp() + "'" +
            "}";
    }

}
