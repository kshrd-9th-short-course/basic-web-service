package com.chanchhaya.restapi.shared.components;

public class ApiConstants {
    
    public static final String API_VERSION = "/api/v1";
    
    public static final String API_DOC_TITLE = "Korea Software HRD Center";

    public static final String API_DOC_DESCRIPTION = "Open Source API Documentation for practice";

    public static final String API_END_POINT_ARTICLES = "/articles";

    public static final String API_END_POINT_CATEGORIES = "/categories";

    public static final String API_END_POINT_IMAGES = "/images";

}
