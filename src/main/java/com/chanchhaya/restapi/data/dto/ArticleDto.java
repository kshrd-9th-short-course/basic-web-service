package com.chanchhaya.restapi.data.dto;

import java.io.Serializable;

import com.chanchhaya.restapi.data.domain.Category;

public class ArticleDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6857605769203162238L;
    private int id;
    private String uuid;
    private String title;
    private String description;
    private String content;
    private String thumbnail;
    private CategoryDto category;
    private boolean status;


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public boolean isStatus() {
        return this.status;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + id + "'" +
            ", uuid='" + uuid + "'" +
            ", title='" + title + "'" +
            ", description='" + description + "'" +
            ", content='" + content + "'" +
            ", thumbnail='" + thumbnail + "'" +
            ", category='" + category + "'" +
            ", status='" + status + "'" +
            "}";
    }
    
}
