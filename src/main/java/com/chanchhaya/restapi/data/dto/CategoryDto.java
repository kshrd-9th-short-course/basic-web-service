package com.chanchhaya.restapi.data.dto;

public class CategoryDto {
    
    private int id;
    private String name;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
        ", id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }

}
