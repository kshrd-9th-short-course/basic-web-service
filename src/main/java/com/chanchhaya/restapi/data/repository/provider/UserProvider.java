package com.chanchhaya.restapi.data.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    
    public String findByUsernameSql() {
        return new SQL(){{
            SELECT("*");
            FROM("users");
            WHERE("username = #{username}");
            AND();
            WHERE("status = TRUE");
        }}.toString();
    }

}
