package com.chanchhaya.restapi.data.repository;

import java.util.List;

import com.chanchhaya.restapi.data.domain.Article;
import com.chanchhaya.restapi.data.domain.Category;
import com.chanchhaya.restapi.data.repository.provider.ArticleProvider;
import com.chanchhaya.restapi.data.repository.provider.CategoryProvider;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository {
    
    @SelectProvider(type = ArticleProvider.class, method = "findAllSql")
    @Results(id = "articleMapper", value = {
        @Result(property = "category", column = "category_id", one = @One(select = "findCategoryById"))
    })
    List<Article> findAll();

    @SelectProvider(type = ArticleProvider.class, method = "findByUUIDSql")
    @ResultMap(value = "articleMapper")
    Article findByUUID(String uuid);

    @InsertProvider(type = ArticleProvider.class, method = "saveSql")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    boolean save(Article article);

    @UpdateProvider(type = ArticleProvider.class, method = "updateSql")
    boolean update(Article article);

    // relationship with category by using id
    @SelectProvider(type = CategoryProvider.class, method = "findCategoryByIdSql")
    @Results(id = "categoryMapper", value = {
        @Result(property = "name", column = "cate_name")
    })
    Category findCategoryById(int id);

    @UpdateProvider(type = ArticleProvider.class, method = "updateStatusToFalseSql")
    boolean updateStatusToFalse(String uuid);

    @DeleteProvider(type = ArticleProvider.class, method = "deleteByUUIDSql")
    boolean deleteByUUID(String uuid);

}
