package com.chanchhaya.restapi.data.repository;

import com.chanchhaya.restapi.data.domain.User;
import com.chanchhaya.restapi.data.repository.provider.UserProvider;

import org.apache.ibatis.annotations.SelectProvider;

public interface UserRepository {
    
    @SelectProvider(type = UserProvider.class, method = "findByUsernameSql")
    User findByUsername();

}
