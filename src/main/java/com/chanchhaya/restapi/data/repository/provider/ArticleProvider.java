package com.chanchhaya.restapi.data.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {
    
    public String findAllSql() {
        return new SQL(){{
            SELECT("*");
            FROM("articles");
            WHERE("status = true");
        }}.toString();
    }

    public String findByUUIDSql() {
        return new SQL() {{
            SELECT("*");
            FROM("articles");
            WHERE("uuid = #{uuid}");
            AND();
            WHERE("status = TRUE");
        }}.toString();
    }

    public String saveSql() {
        return new SQL(){{
            INSERT_INTO("articles");
            VALUES("uuid", "#{uuid}");
            VALUES("title", "#{title}");
            VALUES("description", "#{description}");
            VALUES("thumbnail", "#{thumbnail}");
            VALUES("content", "#{content}");
            VALUES("category_id", "#{category.id}");
        }}.toString();
    }

    public String updateSql() {
        return new SQL(){{
            UPDATE("articles");
            SET("title = #{title}");
            SET("description = #{description}");
            SET("thumbnail = #{thumbnail}");
            SET("content = #{content}");
            SET("category_id = #{category.id}");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String updateStatusToFalseSql() {
        return new SQL(){{
            UPDATE("articles");
            SET("status = FALSE");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

    public String deleteByUUIDSql() {
        return new SQL() {{
            DELETE_FROM("articles");
            WHERE("uuid = #{uuid}");
        }}.toString();
    }

}
