package com.chanchhaya.restapi.data.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String findCategoryByIdSql() {
        return new SQL(){{
            SELECT("*");
            FROM("categories");
            WHERE("id = #{id}");
        }}.toString();
    }
    
}
