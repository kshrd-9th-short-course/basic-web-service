package com.chanchhaya.restapi.data.domain;

public class User {

    private int id;
    private String uuid;
    private String username;
    private String email;
    private String password;
    private boolean status = true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", uuid='" + uuid + '\'' + ", username='" + username + '\'' + ", email='" + email
                + '\'' + ", password='" + password + '\'' + ", status=" + status + '}';
    }


}
