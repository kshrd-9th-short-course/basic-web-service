package com.chanchhaya.restapi.data.domain;

public class Article {
    
    private int id;
    private String uuid;
    private String title;
    private String description;
    private String content;
    private String thumbnail;
    private Category category;
    private boolean status;


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public boolean isStatus() {
        return this.status;
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + id + "'" +
            ", uuid='" + uuid + "'" +
            ", title='" + title + "'" +
            ", description='" + description + "'" +
            ", content='" + content + "'" +
            ", thumbnail='" + thumbnail + "'" +
            ", category='" + category + "'" +
            ", status='" + status + "'" +
            "}";
    }

}
