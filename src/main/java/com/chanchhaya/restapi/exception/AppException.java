package com.chanchhaya.restapi.exception;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.chanchhaya.restapi.rest.response.ErrorResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

@RestControllerAdvice
public class AppException {
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse<List<Map<String,String>>>> handleRestException(MethodArgumentNotValidException e) {

        System.out.println("Exception is worked");

        ErrorResponse<List<Map<String,String>>> response = new ErrorResponse<>();
        List<Map<String,String>> errors = new ArrayList<>();
        Map<String,String> messageOfFields = new HashMap<>();

        e.getBindingResult().getAllErrors().forEach((error) -> {
            messageOfFields.put(((FieldError)error).getField(), error.getDefaultMessage());
        });

        errors.add(messageOfFields);

        response.setMessage(e.getMessage());
        response.setCode(4000);
        response.setErrors(errors);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(value = ResponseStatusException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ResponseEntity<ErrorResponse<String>> handle(ResponseStatusException e) {

        ErrorResponse<String> response = new ErrorResponse<>();

        response.setMessage(e.getLocalizedMessage());
        response.setCode(e.getStatus().value());
        response.setErrors(e.getReason());
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);

    }

}
